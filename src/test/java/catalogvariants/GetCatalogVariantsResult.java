package catalogvariants;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogVariantsResult {
	@SerializedName("isSuccess")
	@Expose
	private Boolean isSuccess;
	@SerializedName("data")
	@Expose
	private GetCatalogVariantsData data;

	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetCatalogVariantsData getData() {
		return data;
	}

	public void setData(GetCatalogVariantsData data) {
		this.data = data;
	}

}
