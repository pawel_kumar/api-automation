package catalogvariants;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCatalogVariantsJson {
	@SerializedName("result")
	@Expose
	private PostCatalogVariantsResults result;

	public PostCatalogVariantsResults getResult() {
		return result;
	}

	public void setResult(PostCatalogVariantsResults result) {
		this.result = result;
	}
}
