package catalogvariants;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCatalogVariantsResults {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private PostCatalogVariantsData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public PostCatalogVariantsData getData() {
		return data;
	}

	public void setData(PostCatalogVariantsData data) {
		this.data = data;
	}

}
