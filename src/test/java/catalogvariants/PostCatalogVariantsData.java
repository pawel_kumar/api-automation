package catalogvariants;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCatalogVariantsData {
	@SerializedName("items")
	@Expose
	private List<Object> items = null;
	@SerializedName("total")
	@Expose
	private Integer total;
	@SerializedName("page")
	@Expose
	private String page;
	@SerializedName("limit")
	@Expose
	private String limit;

	public List<Object> getItems() {
		return items;
	}

	public void setItems(List<Object> items) {
		this.items = items;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

}
