package catalogvariants;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogVariantsCategories {
	@SerializedName("Buahkusayang")
	@Expose
	private Integer buahkusayang;
	@SerializedName("Fruits")
	@Expose
	private Integer fruits;
	@SerializedName("Nyoba Aja")
	@Expose
	private Integer nyobaAja;

	public Integer getBuahkusayang() {
		return buahkusayang;
	}

	public void setBuahkusayang(Integer buahkusayang) {
		this.buahkusayang = buahkusayang;
	}

	public Integer getFruits() {
		return fruits;
	}

	public void setFruits(Integer fruits) {
		this.fruits = fruits;
	}

	public Integer getNyobaAja() {
		return nyobaAja;
	}

	public void setNyobaAja(Integer nyobaAja) {
		this.nyobaAja = nyobaAja;
	}

}
