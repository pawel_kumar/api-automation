package catalogvariants;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogVariantsJson {

	@SerializedName("result")
	@Expose
	private GetCatalogVariantsResult result;

	public GetCatalogVariantsResult getResult() {
		return result;
	}

	public void setResult(GetCatalogVariantsResult result) {
		this.result = result;
	}

}
