package catalogapitest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.PostCatalogVariantsCreateJson;
import catalogvariants.PostCatalogVariantsJson;
import utilbase.TestUtil;

public class PostCatalogVariants extends TestSuiteBase {

	private String v2URL = "/v1/catalog/variants";

	//PostCatalogVariants api create user catalog based on user information like delivery date, Province,UId & type of template. 
	@Test(dataProvider = "getTestData")
	public void postCatalogVariantsTest(String Province, String limit, String page, String aliasQuery, String uid,
			String value, String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		requestParams = PostCatalogVariantsCreateJson.catalogVariantsJson(Province, limit, page, aliasQuery, uid,
				value);
		String FinalURL = getBaseURL("catalog") + v2URL;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").body(requestParams).when().post();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		PostCatalogVariantsJson PostCatalogVariantsJsonObject;
		String adduserResponse = response.body().asString();
		PostCatalogVariantsJsonObject = gson.fromJson(adduserResponse, PostCatalogVariantsJson.class);
		
		Assert.assertEquals(PostCatalogVariantsJsonObject.getResult().getIsSuccess(), "true");
		Assert.assertNotNull(PostCatalogVariantsJsonObject.getResult().getData().getItems(), "Item is null");
		Assert.assertNotNull(PostCatalogVariantsJsonObject.getResult().getData().getLimit(), "Limit is null");
		Assert.assertNotNull(PostCatalogVariantsJsonObject.getResult().getData().getPage(), "Page is null");
		Assert.assertNotNull(PostCatalogVariantsJsonObject.getResult().getData().getTotal(), "Total is null");
	

		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
