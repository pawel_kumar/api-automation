package catalogapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogCategoriesDetailJson;
import utilbase.TestUtil;

public class GetCatalogCategoriesDetail extends TestSuiteBase {
	private String v2URL = "/v1/catalog/categories";
	GetCatalogCategoriesDetailJson GetCatalogCategoriesDetailJsonObject = null;

	//GetCatalogCatagoriesDetails api provide us information about particular catagory based on catagory name. 
	@Test(dataProvider = "getTestData")
	public void getCatalogCategoriesDetailTest(String URL, String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		String FinalURL = getBaseURL("catalog") + v2URL + "/" + URL;
		request = given().baseUri(FinalURL);
		response = request.when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogCategoriesDetailJsonObject = gson.fromJson(adduserResponse, GetCatalogCategoriesDetailJson.class);
		Assert.assertEquals(GetCatalogCategoriesDetailJsonObject.getResult().getIsSuccess(), status);
		
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getCode(), "Code is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getCreatedBy(), "Created by null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getDisplayName(), "Display name null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getDisplayScore(), "Display Score is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getIcon(), "Icon is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getIsActive(), "Is active null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getIsFromOss(), "Is from oss null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getIsNeedAttention(), "Is need attention null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getName(), "Name is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getShortDesc(), "Short desc is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getThumbnail(), "Thumbnail is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getUpdatedBy(), "updated by is null");
		Assert.assertNotNull(GetCatalogCategoriesDetailJsonObject.getResult().getData().getUuid(), "UUID is nul");


		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
