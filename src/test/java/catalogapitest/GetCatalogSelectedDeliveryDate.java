package catalogapitest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogSelectedDeliveryDateJson;
import utilbase.TestUtil;

public class GetCatalogSelectedDeliveryDate extends TestSuiteBase {
	private String v2URL = "/v1/catalog/delivery/user";
	GetCatalogSelectedDeliveryDateJson GetCatalogSelectedDeliveryDateObject = null;

	//GetCatalogSelectedDeliveryDate api provide user selected delivery dates, area & code based on user UID. 
	@Test(dataProvider = "getTestData")
	public void getCatalogSelectedDeliveryDateTest(String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + v2URL + Tookan;
		request = given().baseUri(FinalURL);
		response = request.queryParam("isLegacy", true).when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogSelectedDeliveryDateObject = gson.fromJson(adduserResponse, GetCatalogSelectedDeliveryDateJson.class);
		Assert.assertEquals(GetCatalogSelectedDeliveryDateObject.getResult().getIsSuccess(), status);
		Assert.assertNotNull(GetCatalogSelectedDeliveryDateObject.getResult().getData().getDeliveryArea(), "Delivery Area is null");
		Assert.assertNotNull(GetCatalogSelectedDeliveryDateObject.getResult().getData().getDeliveryCode(), "Delivery Code is null");
		Assert.assertNotNull(GetCatalogSelectedDeliveryDateObject.getResult().getData().getDeliveryDate(), "Delivery date null");
		Assert.assertNotNull(GetCatalogSelectedDeliveryDateObject.getResult().getData().getProvince(), "Province is null");

		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
