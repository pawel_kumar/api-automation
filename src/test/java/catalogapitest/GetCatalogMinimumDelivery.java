package catalogapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogMinimumDeliveryJson;
import utilbase.TestUtil;

public class GetCatalogMinimumDelivery extends TestSuiteBase {

	private String v2URL = "/v1/catalog/delivery/free-delivery";
	GetCatalogMinimumDeliveryJson GetCatalogMinimumDeliveryObject = null;

	//GetcatalogMinimumDelivery api provide area, dates where we have free delivery. 
	@Test(dataProvider = "getTestData")
	public void getCatalogMinimumDeliveryTest(String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + v2URL;
		request = given().baseUri(FinalURL);
		response = request.when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogMinimumDeliveryObject = gson.fromJson(adduserResponse, GetCatalogMinimumDeliveryJson.class);
		Assert.assertEquals(GetCatalogMinimumDeliveryObject.getResult().getIsSuccess(), status);
		Assert.assertNotNull(GetCatalogMinimumDeliveryObject.getResult().getData(), "Data null.");
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
