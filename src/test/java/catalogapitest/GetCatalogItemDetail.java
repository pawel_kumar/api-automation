package catalogapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogItemDetailJson;
import utilbase.TestUtil;

public class GetCatalogItemDetail extends TestSuiteBase {
	private String v2URL = "/v1/catalog/variants/description/";
	GetCatalogItemDetailJson GetCatalogItemDetailJsonObject = null;

	//Getcatalogdetail api provide detail of the particular item. 
	@Test(dataProvider = "getTestData")
	public void getCatalogItemDetailTest(String item, String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + v2URL + item;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogItemDetailJsonObject = gson.fromJson(adduserResponse, GetCatalogItemDetailJson.class);
		Assert.assertEquals(status, GetCatalogItemDetailJsonObject.getResult().getIsSuccess());
		Assert.assertNotNull(GetCatalogItemDetailJsonObject.getResult().getData().getCreatedBy(), "Created by is null");
		Assert.assertNotNull(GetCatalogItemDetailJsonObject.getResult().getData().getCreatedTime(), "Created time null");
		Assert.assertNotNull(GetCatalogItemDetailJsonObject.getResult().getData().getHealthBenefit(), "Health benifit is null");
		Assert.assertNotNull(GetCatalogItemDetailJsonObject.getResult().getData().getIntroduction(), "Introduction null");
		Assert.assertNotNull(GetCatalogItemDetailJsonObject.getResult().getData().getName(), "Name is null");
		Assert.assertNotNull(GetCatalogItemDetailJsonObject.getResult().getData().getStoragePrep(), "Storage prep null");

		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
