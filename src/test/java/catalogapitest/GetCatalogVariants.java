package catalogapitest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogvariants.GetCatalogVariantsJson;
import utilbase.TestUtil;

public class GetCatalogVariants extends TestSuiteBase {
	private String v2URL = "/v1/catalog/variants/detail/";
	GetCatalogVariantsJson GetCatalogVariantsJsonObject;

	@Test(dataProvider = "getTestData")
	public void getCatalogVariantsTest(String Province, String limit, String page, String aliasQuery, String uid,
			String value, String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		String FinalURL = getBaseURL("catalog") + v2URL;
		request = given().baseUri(FinalURL);
		System.out.println("FinalURL"+FinalURL);
		
		
		response = request.contentType("application/json").when().get();
		System.out.println(response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogVariantsJsonObject = gson.fromJson(adduserResponse, GetCatalogVariantsJson.class);
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		Assert.assertEquals(status, GetCatalogVariantsJsonObject.getResult().getIsSuccess());
/*
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getActualPrice());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getAvailability());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getCategories());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getCode());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getCompetitorPrice());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getCreatedBy());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getDeliveryArea());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getDescription());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getDimensionScore());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getFarmers());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getHealthBenefit());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getImage());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getIntroduction());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getIsActive());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getIsBeverage());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getIsDiscount());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getIsProduct());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getIsSuperSensitive());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getLabelDesc());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getLabelDescription());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getLabelName());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getLabels());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getLastUpdateBy());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getMaxQty());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getName());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getNextAvailableDates());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getOrderQuantity());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getPackDesc());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getPackNote());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getPrice());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getScore());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getSearchWords());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getShortDesc());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getSlug());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getStackPosition());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getStockAvailable());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getStoragePrep());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getTotalQuantity());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getType());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getUuid());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getWarehouse());
		Assert.assertNotNull(GetCatalogVariantsJsonObject.getResult().getData().getWeightScore());


*/


		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
