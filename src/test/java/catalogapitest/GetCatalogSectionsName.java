package catalogapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import apiinfo.CatalogApiInfo;
import catalogapipages.GetCatalogSectionsNameJson;
import utilbase.TestUtil;

public class GetCatalogSectionsName extends TestSuiteBase {
	String FinalURL = null;
	GetCatalogSectionsNameJson GetCatalogSectionsNameJsonObject;

	GetCatalogSectionsNameJson getCatalogSectionsNameFun() {
		FinalURL = getBaseURL("catalog") + CatalogApiInfo.CatalogSectionsNameURL;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		String adduserResponse = response.body().asString();
		GetCatalogSectionsNameJsonObject = gson.fromJson(adduserResponse, GetCatalogSectionsNameJson.class);
		return GetCatalogSectionsNameJsonObject;
	}

	//GetCatalogSectionsName api will provide the complete section information based on section name like 'default'
	@Test(dataProvider = "getTestData")
	public void getCatalogSectionsNameTest(String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		GetCatalogSectionsNameJsonObject = getCatalogSectionsNameFun();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		Assert.assertEquals(status, GetCatalogSectionsNameJsonObject.getResult().getIsSuccess());
		Assert.assertEquals("default", GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getSectionName());
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getCountdown(), "Count Down is null.");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getCreatedAt(), "CReated at is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getCreatedBy(), "Created by is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getDisplayScore(), "Display score is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getImageUrl(), "Image url is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getIsActive(), "Is active null. ");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getLayoutType(), "Layout type is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getLinkUrl(), "Link url is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getPromoEnd(), "Promo end is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getPromoStart(), "Promo start is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getSectionName(), "section name is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getSubTitle(), "sub title is null.");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getTitle(), "Title is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getUpdatedAt(), "Updated at is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getUpdatedBy(), "Updated by is null");
		Assert.assertNotNull(GetCatalogSectionsNameJsonObject.getResult().getData().get(0).getUuid(), "UUID is null");






		
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
