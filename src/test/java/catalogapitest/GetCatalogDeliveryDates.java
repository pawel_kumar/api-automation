package catalogapitest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogDeliveryDatesJson;
import utilbase.TestUtil;

public class GetCatalogDeliveryDates extends TestSuiteBase {
	private String v2URL = "/v1/catalog/delivery/dates";
	GetCatalogDeliveryDatesJson GetCatalogDeliveryDatesJsonObject = null;

	//GetCatalogDeliveryDates api provides us delivery dates according to delivery area & code. 
	@Test(dataProvider = "getTestData")
	public void getCatalogDeliveryDatesTest(String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + v2URL;
		request = given().baseUri(FinalURL);
		response = request.when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogDeliveryDatesJsonObject = gson.fromJson(adduserResponse, GetCatalogDeliveryDatesJson.class);
		Assert.assertEquals(GetCatalogDeliveryDatesJsonObject.getResult().getIsSuccess(), status);
		Assert.assertNotNull(GetCatalogDeliveryDatesJsonObject.getResult().getData().getCatalogCoverageArea().get(0).getDeliveryArea(), "Delivery Area is null");
		Assert.assertNotNull(GetCatalogDeliveryDatesJsonObject.getResult().getData().getCatalogCoverageArea().get(0).getDeliveryCode(), "Delivery Code is null");
		Assert.assertNotNull(GetCatalogDeliveryDatesJsonObject.getResult().getData().getCatalogCoverageArea().get(0).getDeliveryDates(), "Delivery Dates are null");
	

		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
