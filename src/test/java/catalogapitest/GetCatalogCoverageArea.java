package catalogapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogCoverageAreaJson;
import utilbase.TestUtil;

public class GetCatalogCoverageArea extends TestSuiteBase {
	private String v2URL = "/v1/catalog/delivery/coverage-area";
	GetCatalogCoverageAreaJson GetCatalogCoverageAreaJsonObject = null;

	//Getcatalogcoverage user provide all areas where we are providing services with dates. 
	@Test(dataProvider = "getTestData")
	public void getCatalogCoverageAreaTest(String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + v2URL;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogCoverageAreaJsonObject = gson.fromJson(adduserResponse, GetCatalogCoverageAreaJson.class);
		Assert.assertEquals(status, GetCatalogCoverageAreaJsonObject.getResult().getIsSuccess());
		Assert.assertNotNull(GetCatalogCoverageAreaJsonObject.getResult().getData().get(0).getDeliveryArea(), "Delivery Area is null");
		Assert.assertNotNull(GetCatalogCoverageAreaJsonObject.getResult().getData().get(0).getDeliveryDates(), "Delivery Dates are null");

		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
