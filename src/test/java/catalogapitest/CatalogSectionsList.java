package catalogapitest;

import static io.restassured.RestAssured.given;

import java.util.List;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import apiinfo.CatalogApiInfo;
import catalogapipages.CatalogSectionsListJson;
import utilbase.TestUtil;

public class CatalogSectionsList extends TestSuiteBase {
	CatalogSectionsListJson CatalogSectionsListJsonObject = null;

	
	//GetCatalogSectionList api provide us complete section list. 
	@Test(dataProvider = "getTestData")
	public void catalogSectionsListTest(String data, String isSuccess, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + CatalogApiInfo.CatalogSectionsListURL;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		CatalogSectionsListJsonObject = gson.fromJson(adduserResponse, CatalogSectionsListJson.class);
		List<String> li = CatalogSectionsListJsonObject.getResult().getData();
		Assert.assertEquals(li.toString().replace("[", "").replace("]", ""), data);
		Assert.assertEquals(CatalogSectionsListJsonObject.getResult().getIsSuccess(), isSuccess);
		Assert.assertNotNull(CatalogSectionsListJsonObject.getResult().getData(), "Data is null");
		

		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
