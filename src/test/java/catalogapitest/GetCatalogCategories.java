package catalogapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import catalogapipages.GetCatalogCategoriesJson;
import utilbase.TestUtil;

public class GetCatalogCategories extends TestSuiteBase {
	private String v2URL = "/v1/catalog/categories";
	GetCatalogCategoriesJson GetCatalogCategoriesJsonObject = null;

	//GetCatalogCatagroies api will return all catagories and there information. 
	@Test(dataProvider = "getTestData")
	public void getCatalogCategoriesTest(String status, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String FinalURL = getBaseURL("catalog") + v2URL;
		request = given().baseUri(FinalURL);
		response = request.when().get();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		String adduserResponse = response.body().asString();
		GetCatalogCategoriesJsonObject = gson.fromJson(adduserResponse, GetCatalogCategoriesJson.class);
		Assert.assertEquals(GetCatalogCategoriesJsonObject.getResult().getIsSuccess(), status);
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getDisplayName(), "Display name is null");
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getDisplayScore(), "Display scroe is null");
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getIcon(), "Icon is null");
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getIsActive(), "Is active null");
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getName(), "name is null");
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getShortDesc(), "Short Desc is null");
		Assert.assertNotNull(GetCatalogCategoriesJsonObject.getResult().getData().get(0).getThumbnail(), "Thumbnail is null");


		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(CatalogSuite_xls, this.getClass().getSimpleName());
	}

}
