package userapitest;

import static io.restassured.RestAssured.given;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import userapipages.PostDeliverySelectionJson;
import utilbase.TestUtil;

public class PostDeliverySelection extends TestSuiteBase {
	//PostDeliverySelection api save user delivery area under user UID. 
	@Test(dataProvider = "getTestData")
	public void postDeliverySelectionTest(String deliveryArea, String deliveryCode, String deliveryDate,
			String description, String isSuccess) {

		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		requestParams = PostDeliverySelectionJson.NewPostDeliverySelectionJson(deliveryArea, deliveryDate,
				deliveryCode);
		String FinalURL = getBaseURL("user") + user_api_URL.delivery_selection_URL + "/" + user_api_URL.UID + "/"
				+ user_api_URL.provider;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").body(requestParams).when().post();
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		
		
		
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

}
