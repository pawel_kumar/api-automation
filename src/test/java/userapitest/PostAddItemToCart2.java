package userapitest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import userapipages.PostAddItemToCartCreateRequest;
import userapipages.PostAddItemToCartJson;
import utilbase.TestUtil;

public class PostAddItemToCart2 extends TestSuiteBase {
	String FinalURL = null;
	private String name;
	private String quantity;

	@Factory(dataProvider = "getTestData2")
	public PostAddItemToCart2(String name, String quantity) {
		this.name = name;
		this.quantity = quantity;

	}

	PostAddItemToCartJson PostAddItemToCartJsonObject;

	PostAddItemToCartJson postAddUserfun(String name, String quantity, String UID, String Provider) {
		requestParams = PostAddItemToCartCreateRequest.createJSON(name, quantity);
		FinalURL = getBaseURL("user") + user_api_URL.add_user_URL + "/" + UID + "/" + Provider;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").body(requestParams).when().post();
		String adduserResponse = response.body().asString();
		PostAddItemToCartJsonObject = gson.fromJson(adduserResponse, PostAddItemToCartJson.class);
		return PostAddItemToCartJsonObject;
	}

	@Test(dataProvider = "getTestData")
	public void postAddUserTest(String name, String quantity, String status, String error, String description) {

		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String UID = user_api_URL.UID;
		String Provider = user_api_URL.provider;
		PostAddItemToCartJsonObject = postAddUserfun(name, quantity, UID, Provider);
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		Assert.assertEquals(PostAddItemToCartJsonObject.getResult().getIsSuccess(), status);
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
		if (status.equalsIgnoreCase("false")) {
			verifyErrorMsg(error);
		}

	}

	@Test(dataProvider = "getTestData")
	public void PostAddUserUIDTest(String name, String quantity, String status, String error, String description) {

		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}

		String UID = "sjdkdmnsj";
		String Provider = user_api_URL.provider;
		PostAddItemToCartJsonObject = postAddUserfun(name, quantity, UID, Provider);
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		Assert.assertEquals(PostAddItemToCartJsonObject.getResult().getIsSuccess(), status);
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
		if (status.equalsIgnoreCase("false")) {
			verifyErrorMsg(error);
		}
	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

	@DataProvider
	public Object[][] getTestData2() {
		return TestUtil.getData(UserSuite_xls, "PostAddItemToCart2");
	}

}
