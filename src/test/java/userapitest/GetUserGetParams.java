package userapitest;

import org.testng.annotations.Test;

import userapipages.GetUserGetParamsJson;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import utilbase.TestUtil;

public class GetUserGetParams extends TestSuiteBase {

	public GetUserGetParamsJson GetUserGetParamsJsonObject = null;

	GetUserGetParamsJson getUserGetParamsFun() {

		String FinalURL = getBaseURL("user") + user_api_URL.GetUserGetParams;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		String GetDeliverySelectionResponse = response.body().asString();
		GetUserGetParamsJsonObject = gson.fromJson(GetDeliverySelectionResponse, GetUserGetParamsJson.class);
		return GetUserGetParamsJsonObject;

	}
	//GetUserGetParam api will provide parameter to get feedback based on score, name, description. 
	@Test(dataProvider = "getTestData")
	public void getUserGetParamsTest(String isSuccess, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		APP_LOGS.debug(" Executing TestCase_A1");
		GetUserGetParamsJsonObject = getUserGetParamsFun();
		test = extent.createTest("Get Delivery Selection Status Test", response.prettyPrint());
		Assert.assertEquals(GetUserGetParamsJsonObject.getResult().getIsSuccess().toString(), isSuccess);
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

}
