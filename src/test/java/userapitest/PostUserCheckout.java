package userapitest;

import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import userapipages.PostUserCheckoutRequestJson;
import userapipages.postusercheckout.PostUserCheckoutJson;
import utilbase.TestUtil;

public class PostUserCheckout extends TestSuiteBase {

	String FinalURL = null;
	PostUserCheckoutJson PostUserCheckoutJsonObject;

	PostUserCheckoutJson postUserCheckoutFun(String provider) {

		requestParams = PostUserCheckoutRequestJson.createJSON(user_api_URL.UID, provider);
		FinalURL = getBaseURL("user") + user_api_URL.PostUserCheckout;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").body(requestParams).when().post();
		String adduserResponse = response.body().asString();
		PostUserCheckoutJsonObject = gson.fromJson(adduserResponse, PostUserCheckoutJson.class);
		return PostUserCheckoutJsonObject;
	}
	//PostUserCheckout api will checkout the user selected items based on user UID. 
	@Test(dataProvider = "getTestData")
	public void postUserCheckoutStatusTest(String provider, String status, String description) {
		// test the runmode of current dataset
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		PostUserCheckoutJsonObject = postUserCheckoutFun(provider);
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams
				+ System.lineSeparator() + "Response-" + response.prettyPrint());
		Assert.assertEquals(status, PostUserCheckoutJsonObject.getResult().getIsSuccess());
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getCheckoutTimestamp(), "Checkout Timestamp is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getDeliveryCode(), "Delivery Code is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getDeliveryDate(), "Delivery Date is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getOrderNo(), "Order no is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getTotal().getDeliveryFee(), "Delivery fee is null");	
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getTotal().getDiscountAmount(), "Discount amount  is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getTotal().getIsFreeDelivery(), "Free Delivery is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getTotal().getTotalPrice(), "Total price is null");
		Assert.assertNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getErrorMessage(), "Error Message is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getActualPrice(), "Actual price(is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getAvailability(), "Availability is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getCategories().getBuahkusayang(), "Buahkusayang is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getCategories().getNyobaAja(), "NyobaAja is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getCategories().getPreparedFoodsDrinks(), "Prepared Foods Drinksis null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getCode(), "Code is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getCompetitorPrice(), "Competitor price is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getCreatedBy(), "CreatedBy is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getDeliveryArea(), "DeliveryArea is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getDescription(), "Description is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getDimensionScore(), "DimensionScore is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getDisplayName(), "DisplayName is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getDisplayScore(), "DisplayScore is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getFarmers().get(0).getImage(), "Image is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getFarmers().get(0).getBlogLink(), "blog linkis null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getFarmers().get(0).getName(), "name is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getFarmers().get(0).getShortDesc(), "Short desc is null");
		Assert.assertNotNull(PostUserCheckoutJsonObject.getResult().getData().getItems().get(0).getFarmers().get(0).getId(), "farmer id is null");
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();
	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

}
