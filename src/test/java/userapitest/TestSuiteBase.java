package userapitest;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.gson.Gson;
import base.TestBase;
import userapipages.ResponseClass;
import utilbase.TestUtil;

public class TestSuiteBase extends TestBase {

	protected io.restassured.response.Response response;
	protected io.restassured.response.ValidatableResponse json;
	protected io.restassured.specification.RequestSpecification request;
	static boolean fail = false;
	static boolean skip = false;
	static boolean isTestPass = true;
	static int count = -1;
	static String runmodes[] = null;
	static JSONObject requestParams = null;
	public ResponseClass response_object = null;
	Gson gson = new Gson();

	@AfterMethod
	public void reportDataSetResult(ITestResult result) {
		if (skip)
			TestUtil.reportDataSetResult(UserSuite_xls, this.getClass().getSimpleName(), count + 2, "SKIP");
		else if (fail) {
			isTestPass = false;
			TestUtil.reportDataSetResult(UserSuite_xls, this.getClass().getSimpleName(), count + 2, "FAIL");
		} else
			TestUtil.reportDataSetResult(UserSuite_xls, this.getClass().getSimpleName(), count + 2, "PASS");

		skip = false;
		fail = false;

		if (result.getStatus() == ITestResult.FAILURE) {

			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " FAILED ", ExtentColor.RED));
			test.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
		} else {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
			test.skip(result.getThrowable());
		}

		tearDown();

	}

	@BeforeClass
	public void checkTestSkip() {

		if (!TestUtil.isTestCaseRunnable(UserSuite_xls, this.getClass().getSimpleName())) {
			APP_LOGS.debug("Skipping Test Case" + this.getClass().getSimpleName() + " as runmode set to NO");// logs
			throw new SkipException("Skipping Test Case" + this.getClass().getSimpleName() + " as runmode set to NO");// reports
		}
		runmodes = TestUtil.getDataSetRunmodes(UserSuite_xls, this.getClass().getSimpleName());
	}

	@AfterClass
	public void reportTestResult() {
		if (isTestPass)
			TestUtil.reportDataSetResult(UserSuite_xls, "Test Cases",
					TestUtil.getRowNum(UserSuite_xls, this.getClass().getSimpleName()), "PASS");
		else
			TestUtil.reportDataSetResult(UserSuite_xls, "Test Cases",
					TestUtil.getRowNum(UserSuite_xls, this.getClass().getSimpleName()), "FAIL");

	}

	public void tearDown() {
		fail = false;
		skip = false;
		isTestPass = true;
		count = -1;
		String runmodes[] = null;
		requestParams = null;

		count = -1;
	}

	public void verifySuccess(String expectedSuccess) {
		String adduserResponse = response.body().asString();
		response_object = gson.fromJson(adduserResponse, ResponseClass.class);

	}

	public void verifyErrorMsg(String expectedError) {
		String adduserResponse = response.body().asString();
		response_object = gson.fromJson(adduserResponse, ResponseClass.class);
		Assert.assertEquals(expectedError, response_object.getResult().getError());

	}

	public void verifyStatusCode() {
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
	}

	public void verifyStatusLine() {
		String StatusLine = response.getStatusLine();
		Assert.assertEquals(StatusLine, "HTTP/1.1 200 OK");
	}

	public void verifyHeaderContentType() {
		String ContentType = response.header("Content-Type");
		Assert.assertEquals(ContentType, "application/json; charset=utf-8");
	}

	public void verifyHeaderContentEncoding() {
		String Content_Encoding = response.header("Content-Encoding");
		Assert.assertEquals(Content_Encoding, "gzip");
	}

}
