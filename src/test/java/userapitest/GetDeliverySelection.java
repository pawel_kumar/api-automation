package userapitest;

import org.testng.annotations.Test;
import userapipages.GetDeliverySelectionJson;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import utilbase.TestUtil;

public class GetDeliverySelection extends TestSuiteBase {

	String FinalURL=null;
	public GetDeliverySelectionJson GetDeliverySelectionJsonObject = null;

	GetDeliverySelectionJson getDeliverySelectionFun() {

		FinalURL = getBaseURL("user") + user_api_URL.get_delivery_selection_URL + "/" + user_api_URL.UID + "/"
				+ user_api_URL.provider;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		String GetDeliverySelectionResponse = response.body().asString();
		GetDeliverySelectionJsonObject = gson.fromJson(GetDeliverySelectionResponse, GetDeliverySelectionJson.class);
		return GetDeliverySelectionJsonObject;

	}

	//GetDelivery Selection api will provide us user selected area, date & code on the basis of user UID. 

	@Test(dataProvider = "getTestData")
	public void getUserDeliverySelectionStatusTest(String isSuccess,  String deliveryDate, String deliveryArea, String deliveryCode,
			String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		test = extent.createTest(description, "URL-" + FinalURL + System.lineSeparator() + "Request-" + requestParams+ System.lineSeparator() + "Response-" + response.prettyPrint());

		//test = extent.createTest("Get Delivery Selection Status Test", response.prettyPrint());
		GetDeliverySelectionJsonObject = getDeliverySelectionFun();
		Assert.assertEquals(GetDeliverySelectionJsonObject.getResult().getIsSuccess().toString(), isSuccess);
		Assert.assertNotNull(GetDeliverySelectionJsonObject.getResult().getData().getDeliveryArea(), "Delivery Area is null. ");
		Assert.assertNotNull(GetDeliverySelectionJsonObject.getResult().getData().getDeliveryCode(), "Delivery Code is null. ");
		Assert.assertNotNull(GetDeliverySelectionJsonObject.getResult().getData().getDeliveryDate(), "Delivery Date is null. ");
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@Test(dataProvider = "getTestData")
	public void getUserDeliverySelectionDeliveryDateTest(String isSuccess, String deliveryDate, String deliveryArea, String deliveryCode,
			String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		APP_LOGS.debug(" Executing TestCase_A1");
		GetDeliverySelectionJsonObject = getDeliverySelectionFun();
		test = extent.createTest("Get Delivery Selection Delivery Date Test", response.prettyPrint());
		Assert.assertEquals(GetDeliverySelectionJsonObject.getResult().getData().getDeliveryDate(),
				deliveryDate);
	}

	@Test(dataProvider = "getTestData")
	public void getDeliverySelectionDeliveryCodeTest(String isSuccess, String deliveryDate,  String deliveryArea, String deliveryCode,
			String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		APP_LOGS.debug(" Executing TestCase_A1");
		GetDeliverySelectionJsonObject = getDeliverySelectionFun();
		test = extent.createTest("Get Delivery Selection Delivery Code Test", response.prettyPrint());
		Assert.assertEquals(GetDeliverySelectionJsonObject.getResult().getData().getDeliveryCode(), deliveryCode);

	}

	@Test(dataProvider = "getTestData")
	public void getDeliverySelectionDeliveryAreaTest(String isSuccess, String deliveryDate,  String deliveryArea, String deliveryCode,
			String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		APP_LOGS.debug(" Executing TestCase_A1");
		GetDeliverySelectionJsonObject = getDeliverySelectionFun();
		test = extent.createTest("Get Delivery Selection Delivery Area Test", response.prettyPrint());
		Assert.assertEquals(GetDeliverySelectionJsonObject.getResult().getData().getDeliveryArea(), deliveryArea);

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

}
