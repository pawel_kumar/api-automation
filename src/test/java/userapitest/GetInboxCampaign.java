package userapitest;

import org.testng.annotations.Test;
import userapipages.GetInboxCampaignJson;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import utilbase.TestUtil;

public class GetInboxCampaign extends TestSuiteBase {

	public GetInboxCampaignJson GetInboxCampaignJsonObject = null;

	GetInboxCampaignJson GetInboxCampaignFun() {

		String FinalURL = getBaseURL("user") + user_api_URL.GetInboxCampaign;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		String GetInboxCampaignResponse = response.body().asString();
		GetInboxCampaignJsonObject = gson.fromJson(GetInboxCampaignResponse, GetInboxCampaignJson.class);
		return GetInboxCampaignJsonObject;

	}

	@Test(dataProvider = "getTestData")
	public void GetInboxCampaignTest(String isSuccess, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		APP_LOGS.debug(" Executing TestCase_A1");
		GetInboxCampaignJsonObject = GetInboxCampaignFun();
		test = extent.createTest("Get Inbox Campaign Status Test", response.prettyPrint());
		Assert.assertEquals(GetInboxCampaignJsonObject.getResult().getIsSuccess().toString(), isSuccess);
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

}
