package userapitest;

import org.testng.annotations.Test;
import userapipages.GetUserLastAddressJson;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import utilbase.TestUtil;

public class GetUserLastAddress extends TestSuiteBase {
	public GetUserLastAddressJson GetUserLastAddressJsonObject = null;

	GetUserLastAddressJson getUserLastAddressFun() {

		String FinalURL = getBaseURL("user") + user_api_URL.GetUserLastAddress + "/" + user_api_URL.UID + "/"
				+ user_api_URL.provider;
		request = given().baseUri(FinalURL);
		response = request.contentType("application/json").when().get();
		String GetUserLastAddressResponse = response.body().asString();
		GetUserLastAddressJsonObject = gson.fromJson(GetUserLastAddressResponse, GetUserLastAddressJson.class);
		return GetUserLastAddressJsonObject;

	}
	//GetUserLastAddress api will provide the user last address based on user UID. 
	@Test(dataProvider = "getTestData")
	public void GetUserLastAddressTest(String isSuccess, String description) {
		count++;
		if (!runmodes[count].equalsIgnoreCase("Y")) {
			skip = true;
			throw new SkipException("Runmode for test set data set to no " + count);
		}
		APP_LOGS.debug(" Executing TestCase_A1");
		GetUserLastAddressJsonObject = getUserLastAddressFun();
		test = extent.createTest("Get User Last Address Status Test", response.prettyPrint());
		Assert.assertEquals(GetUserLastAddressJsonObject.getResult().getIsSuccess().toString(), isSuccess);
		verifyStatusLine();
		verifyStatusCode();
		verifyHeaderContentType();

	}

	@DataProvider
	public Object[][] getTestData() {
		return TestUtil.getData(UserSuite_xls, this.getClass().getSimpleName());
	}

}
