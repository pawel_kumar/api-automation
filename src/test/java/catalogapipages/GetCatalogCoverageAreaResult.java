package catalogapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCoverageAreaResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private List<GetCatalogCoverageAreaData> data = null;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public List<GetCatalogCoverageAreaData> getData() {
		return data;
	}

	public void setData(List<GetCatalogCoverageAreaData> data) {
		this.data = data;
	}

}
