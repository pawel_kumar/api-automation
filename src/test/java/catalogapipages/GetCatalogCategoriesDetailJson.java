package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCategoriesDetailJson {

	@SerializedName("result")
	@Expose
	private GetCatalogCategoriesDetailResult result;

	public GetCatalogCategoriesDetailResult getResult() {
		return result;
	}

	public void setResult(GetCatalogCategoriesDetailResult result) {
		this.result = result;
	}

}
