package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogDeliveryDatesJson {

	@SerializedName("result")
	@Expose
	private GetCatalogDeliveryDatesResult result;

	public GetCatalogDeliveryDatesResult getResult() {
		return result;
	}

	public void setResult(GetCatalogDeliveryDatesResult result) {
		this.result = result;
	}
}
