package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogSelectedDeliveryDateResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private GetCatalogSelectedDeliveryDateData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetCatalogSelectedDeliveryDateData getData() {
		return data;
	}

	public void setData(GetCatalogSelectedDeliveryDateData data) {
		this.data = data;
	}

}
