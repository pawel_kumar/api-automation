package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCoverageAreaJson {

	@SerializedName("result")
	@Expose
	private GetCatalogCoverageAreaResult result;

	public GetCatalogCoverageAreaResult getResult() {
		return result;
	}

	public void setResult(GetCatalogCoverageAreaResult result) {
		this.result = result;
	}

}
