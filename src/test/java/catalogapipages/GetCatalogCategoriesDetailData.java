package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCategoriesDetailData {

	@SerializedName("code")
	@Expose
	private String code;
	@SerializedName("createdBy")
	@Expose
	private String createdBy;
	@SerializedName("displayName")
	@Expose
	private String displayName;
	@SerializedName("displayScore")
	@Expose
	private Integer displayScore;
	@SerializedName("icon")
	@Expose
	private String icon;
	@SerializedName("isActive")
	@Expose
	private Boolean isActive;
	@SerializedName("isFromOss")
	@Expose
	private Boolean isFromOss;
	@SerializedName("isNeedAttention")
	@Expose
	private Boolean isNeedAttention;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("shortDesc")
	@Expose
	private String shortDesc;
	@SerializedName("thumbnail")
	@Expose
	private String thumbnail;
	@SerializedName("updatedBy")
	@Expose
	private String updatedBy;
	@SerializedName("uuid")
	@Expose
	private String uuid;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getDisplayScore() {
		return displayScore;
	}

	public void setDisplayScore(Integer displayScore) {
		this.displayScore = displayScore;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFromOss() {
		return isFromOss;
	}

	public void setIsFromOss(Boolean isFromOss) {
		this.isFromOss = isFromOss;
	}

	public Boolean getIsNeedAttention() {
		return isNeedAttention;
	}

	public void setIsNeedAttention(Boolean isNeedAttention) {
		this.isNeedAttention = isNeedAttention;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
