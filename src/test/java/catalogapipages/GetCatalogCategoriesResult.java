package catalogapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCategoriesResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private List<GetCatalogCategoriesData> data = null;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public List<GetCatalogCategoriesData> getData() {
		return data;
	}

	public void setData(List<GetCatalogCategoriesData> data) {
		this.data = data;
	}

}
