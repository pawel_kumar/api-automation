package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogMinimumDeliveryJson {

	@SerializedName("result")
	@Expose
	private GetCatalogMinimumDeliveryResult result;

	public GetCatalogMinimumDeliveryResult getResult() {
		return result;
	}

	public void setResult(GetCatalogMinimumDeliveryResult result) {
		this.result = result;

	}
}
