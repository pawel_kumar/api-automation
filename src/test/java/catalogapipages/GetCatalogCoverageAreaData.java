package catalogapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCoverageAreaData {

	@SerializedName("deliveryArea")
	@Expose
	private String deliveryArea;
	@SerializedName("deliveryDates")
	@Expose
	private List<String> deliveryDates = null;

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

	public List<String> getDeliveryDates() {
		return deliveryDates;
	}

	public void setDeliveryDates(List<String> deliveryDates) {
		this.deliveryDates = deliveryDates;
	}

}
