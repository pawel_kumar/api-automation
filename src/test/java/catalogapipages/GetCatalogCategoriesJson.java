package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCategoriesJson {

	@SerializedName("result")
	@Expose
	private GetCatalogCategoriesResult result;

	public GetCatalogCategoriesResult getResult() {
		return result;
	}

	public void setResult(GetCatalogCategoriesResult result) {
		this.result = result;
	}

}
