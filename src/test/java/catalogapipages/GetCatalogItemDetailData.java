package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogItemDetailData {

	@SerializedName("createdBy")
	@Expose
	private String createdBy;
	@SerializedName("createdTime")
	@Expose
	private String createdTime;
	@SerializedName("healthBenefit")
	@Expose
	private String healthBenefit;
	@SerializedName("introduction")
	@Expose
	private String introduction;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("storagePrep")
	@Expose
	private String storagePrep;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getHealthBenefit() {
		return healthBenefit;
	}

	public void setHealthBenefit(String healthBenefit) {
		this.healthBenefit = healthBenefit;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStoragePrep() {
		return storagePrep;
	}

	public void setStoragePrep(String storagePrep) {
		this.storagePrep = storagePrep;
	}

}
