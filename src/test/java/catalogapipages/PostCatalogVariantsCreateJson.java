package catalogapipages;

import org.json.simple.JSONObject;

import utilbase.TestUtil;

public class PostCatalogVariantsCreateJson {

	public JSONObject createJson(String name, String type, String quantity) {

		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("type", type);
		requestParams.put("quantity", quantity);

		return requestParams;
	}

	public static JSONObject catalogVariantsJson(String Province, String limit, String page, String aliasQuery,
			String uid, String value) {

		JSONObject requestParams = new JSONObject();
		requestParams.put("deliveryDate", TestUtil.getCurrentDate());
		requestParams.put("Province", Province);
		requestParams.put("limit", limit);
		requestParams.put("page", page);
		requestParams.put("aliasQuery", aliasQuery);
		requestParams.put("uid", uid);

		requestParams.put("value", value);

		return requestParams;
	}

	// public

}
