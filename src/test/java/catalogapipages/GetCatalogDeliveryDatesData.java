package catalogapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogDeliveryDatesData {

	@SerializedName("catalogCoverageArea")
	@Expose
	private List<GetCatalogDeliveryDatesArea> catalogCoverageArea = null;

	public List<GetCatalogDeliveryDatesArea> getCatalogCoverageArea() {
		return catalogCoverageArea;
	}

	public void setCatalogCoverageArea(List<GetCatalogDeliveryDatesArea> catalogCoverageArea) {
		this.catalogCoverageArea = catalogCoverageArea;
	}

}
