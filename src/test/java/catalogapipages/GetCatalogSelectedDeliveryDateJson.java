package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogSelectedDeliveryDateJson {

	@SerializedName("result")
	@Expose
	private GetCatalogSelectedDeliveryDateResult result;

	public GetCatalogSelectedDeliveryDateResult getResult() {
		return result;
	}

	public void setResult(GetCatalogSelectedDeliveryDateResult result) {
		this.result = result;
	}

}
