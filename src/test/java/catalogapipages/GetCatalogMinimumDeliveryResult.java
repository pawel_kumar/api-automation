package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogMinimumDeliveryResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private Integer data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Integer getData() {
		return data;
	}

	public void setData(Integer data) {
		this.data = data;
	}

}
