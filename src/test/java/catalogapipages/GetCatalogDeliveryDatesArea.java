package catalogapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogDeliveryDatesArea {

	@SerializedName("deliveryArea")
	@Expose
	private String deliveryArea;
	@SerializedName("deliveryCode")
	@Expose
	private String deliveryCode;
	@SerializedName("deliveryDates")
	@Expose
	private List<String> deliveryDates = null;

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public List<String> getDeliveryDates() {
		return deliveryDates;
	}

	public void setDeliveryDates(List<String> deliveryDates) {
		this.deliveryDates = deliveryDates;
	}

}
