package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogItemDetailJson {

	@SerializedName("result")
	@Expose
	private GetCatalogItemDetailResult result;

	public GetCatalogItemDetailResult getResult() {
		return result;
	}

	public void setResult(GetCatalogItemDetailResult result) {
		this.result = result;
	}

}
