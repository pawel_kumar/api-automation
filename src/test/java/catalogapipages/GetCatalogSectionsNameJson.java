package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogSectionsNameJson {
	@SerializedName("result")
	@Expose
	private GetCatalogSectionsNameResult result;

	public GetCatalogSectionsNameResult getResult() {
		return result;
	}

	public void setResult(GetCatalogSectionsNameResult result) {
		this.result = result;
	}

}
