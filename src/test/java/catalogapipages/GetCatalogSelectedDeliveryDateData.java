package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogSelectedDeliveryDateData {

	@SerializedName("deliveryDate")
	@Expose
	private String deliveryDate;
	@SerializedName("deliveryArea")
	@Expose
	private String deliveryArea;
	@SerializedName("deliveryCode")
	@Expose
	private String deliveryCode;
	@SerializedName("province")
	@Expose
	private String province;

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}
