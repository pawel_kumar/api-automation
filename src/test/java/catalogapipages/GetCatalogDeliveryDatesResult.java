package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogDeliveryDatesResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private GetCatalogDeliveryDatesData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetCatalogDeliveryDatesData getData() {
		return data;
	}

	public void setData(GetCatalogDeliveryDatesData data) {
		this.data = data;
	}
}
