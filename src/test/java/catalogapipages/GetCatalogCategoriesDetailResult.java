package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCategoriesDetailResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private GetCatalogCategoriesDetailData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetCatalogCategoriesDetailData getData() {
		return data;
	}

	public void setData(GetCatalogCategoriesDetailData data) {
		this.data = data;
	}

}
