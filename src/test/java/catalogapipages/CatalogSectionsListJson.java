package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatalogSectionsListJson {

	@SerializedName("result")
	@Expose
	private CatalogSectionsListResult result;

	public CatalogSectionsListResult getResult() {
		return result;
	}

	public void setResult(CatalogSectionsListResult result) {
		this.result = result;
	}

}
