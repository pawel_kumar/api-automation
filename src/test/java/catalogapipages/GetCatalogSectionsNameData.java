package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogSectionsNameData {
	@SerializedName("subTitle")
	@Expose
	private String subTitle;
	@SerializedName("updatedBy")
	@Expose
	private String updatedBy;
	@SerializedName("displayScore")
	@Expose
	private Integer displayScore;
	@SerializedName("linkUrl")
	@Expose
	private String linkUrl;
	@SerializedName("createdBy")
	@Expose
	private String createdBy;
	@SerializedName("isActive")
	@Expose
	private Boolean isActive;
	@SerializedName("createdAt")
	@Expose
	private double createdAt;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("countdown")
	@Expose
	private Boolean countdown;
	@SerializedName("sectionName")
	@Expose
	private String sectionName;
	@SerializedName("updatedAt")
	@Expose
	private double updatedAt;
	@SerializedName("promoEnd")
	@Expose
	private double promoEnd;
	@SerializedName("imageUrl")
	@Expose
	private String imageUrl;
	@SerializedName("layoutType")
	@Expose
	private Integer layoutType;
	@SerializedName("promoStart")
	@Expose
	private double promoStart;
	@SerializedName("uuid")
	@Expose
	private String uuid;

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getDisplayScore() {
		return displayScore;
	}

	public void setDisplayScore(Integer displayScore) {
		this.displayScore = displayScore;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public double getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Integer createdAt) {
		this.createdAt = createdAt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getCountdown() {
		return countdown;
	}

	public void setCountdown(Boolean countdown) {
		this.countdown = countdown;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public double getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(double updatedAt) {
		this.updatedAt = updatedAt;
	}

	public double getPromoEnd() {
		return promoEnd;
	}

	public void setPromoEnd(double promoEnd) {
		this.promoEnd = promoEnd;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(Integer layoutType) {
		this.layoutType = layoutType;
	}

	public double getPromoStart() {
		return promoStart;
	}

	public void setPromoStart(double promoStart) {
		this.promoStart = promoStart;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
