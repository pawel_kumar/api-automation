package catalogapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogCategoriesData {

	@SerializedName("displayName")
	@Expose
	private String displayName;
	@SerializedName("isActive")
	@Expose
	private Boolean isActive;
	@SerializedName("shortDesc")
	@Expose
	private String shortDesc;
	@SerializedName("displayScore")
	@Expose
	private Integer displayScore;
	@SerializedName("thumbnail")
	@Expose
	private String thumbnail;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("icon")
	@Expose
	private String icon;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Integer getDisplayScore() {
		return displayScore;
	}

	public void setDisplayScore(Integer displayScore) {
		this.displayScore = displayScore;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
