package catalogapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCatalogSectionsNameResult {

	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private List<GetCatalogSectionsNameData> data = null;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public List<GetCatalogSectionsNameData> getData() {
		return data;
	}

	public void setData(List<GetCatalogSectionsNameData> data) {
		this.data = data;
	}

}
