package userapipages.postusercheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutCategories {
	@SerializedName("Buahkusayang")
	@Expose
	private Integer buahkusayang;
	@SerializedName("Nyoba Aja")
	@Expose
	private Integer nyobaAja;
	@SerializedName("Prepared foods & drinks")
	@Expose
	private Integer preparedFoodsDrinks;

	public Integer getBuahkusayang() {
		return buahkusayang;
	}

	public void setBuahkusayang(Integer buahkusayang) {
		this.buahkusayang = buahkusayang;
	}

	public Integer getNyobaAja() {
		return nyobaAja;
	}

	public void setNyobaAja(Integer nyobaAja) {
		this.nyobaAja = nyobaAja;
	}

	public Integer getPreparedFoodsDrinks() {
		return preparedFoodsDrinks;
	}

	public void setPreparedFoodsDrinks(Integer preparedFoodsDrinks) {
		this.preparedFoodsDrinks = preparedFoodsDrinks;
	}

}
