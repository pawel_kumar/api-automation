package userapipages.postusercheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutFarmer {
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("image")
	@Expose
	private String image;
	@SerializedName("blogLink")
	@Expose
	private String blogLink;
	@SerializedName("shortDesc")
	@Expose
	private String shortDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getBlogLink() {
		return blogLink;
	}

	public void setBlogLink(String blogLink) {
		this.blogLink = blogLink;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

}
