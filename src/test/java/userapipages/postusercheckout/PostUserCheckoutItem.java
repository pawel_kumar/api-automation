package userapipages.postusercheckout;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutItem {
	@SerializedName("actualPrice")
	@Expose
	private Integer actualPrice;
	@SerializedName("availability")
	@Expose
	private Boolean availability;
	@SerializedName("categories")
	@Expose
	private PostUserCheckoutCategories categories;
	@SerializedName("competitorPrice")
	@Expose
	private Integer competitorPrice;
	@SerializedName("createdBy")
	@Expose
	private String createdBy;
	@SerializedName("code")
	@Expose
	private Integer code;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("shortDesc")
	@Expose
	private String shortDesc;
	@SerializedName("dimensionScore")
	@Expose
	private Integer dimensionScore;
	@SerializedName("displayName")
	@Expose
	private String displayName;
	@SerializedName("displayScore")
	@Expose
	private Integer displayScore;
	@SerializedName("farmers")
	@Expose
	private List<PostUserCheckoutFarmer> farmers = null;
	@SerializedName("image")
	@Expose
	private PostUserCheckoutImage image;
	@SerializedName("isActive")
	@Expose
	private Boolean isActive;
	@SerializedName("isBeverage")
	@Expose
	private Boolean isBeverage;
	@SerializedName("isDiscount")
	@Expose
	private Boolean isDiscount;
	@SerializedName("isProduct")
	@Expose
	private Boolean isProduct;
	@SerializedName("isSuperSensitive")
	@Expose
	private Boolean isSuperSensitive;
	@SerializedName("labels")
	@Expose
	private Integer labels;
	@SerializedName("labelName")
	@Expose
	private String labelName;
	@SerializedName("labelDescription")
	@Expose
	private String labelDescription;
	@SerializedName("labelDesc")
	@Expose
	private String labelDesc;
	@SerializedName("lastUpdateBy")
	@Expose
	private String lastUpdateBy;
	@SerializedName("lastUpdateAt")
	@Expose
	private Object lastUpdateAt;
	@SerializedName("maxQty")
	@Expose
	private Integer maxQty;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("orderQuantity")
	@Expose
	private Integer orderQuantity;
	@SerializedName("nextAvailableDates")
	@Expose
	private List<String> nextAvailableDates = null;
	@SerializedName("packDesc")
	@Expose
	private String packDesc;
	@SerializedName("packNote")
	@Expose
	private String packNote;
	@SerializedName("price")
	@Expose
	private Integer price;
	@SerializedName("score")
	@Expose
	private String score;
	@SerializedName("searchWords")
	@Expose
	private String searchWords;
	@SerializedName("stackPosition")
	@Expose
	private Integer stackPosition;
	@SerializedName("stockAvailable")
	@Expose
	private Integer stockAvailable;
	@SerializedName("totalQuantity")
	@Expose
	private Integer totalQuantity;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("uuid")
	@Expose
	private String uuid;
	@SerializedName("weightScore")
	@Expose
	private Double weightScore;
	@SerializedName("warehouse")
	@Expose
	private String warehouse;
	@SerializedName("deliveryArea")
	@Expose
	private String deliveryArea;
	@SerializedName("slug")
	@Expose
	private String slug;
	@SerializedName("introduction")
	@Expose
	private String introduction;
	@SerializedName("healthBenefit")
	@Expose
	private String healthBenefit;
	@SerializedName("storagePrep")
	@Expose
	private String storagePrep;
	@SerializedName("isRemoved")
	@Expose
	private Boolean isRemoved;
	@SerializedName("quantity")
	@Expose
	private String quantity;
	@SerializedName("isOutOfStock")
	@Expose
	private Boolean isOutOfStock;
	@SerializedName("errorMessage")
	@Expose
	private Object errorMessage;
	@SerializedName("subTotal")
	@Expose
	private Integer subTotal;

	public Integer getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(Integer actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Boolean getAvailability() {
		return availability;
	}

	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	public PostUserCheckoutCategories getCategories() {
		return categories;
	}

	public void setCategories(PostUserCheckoutCategories categories) {
		this.categories = categories;
	}

	public Integer getCompetitorPrice() {
		return competitorPrice;
	}

	public void setCompetitorPrice(Integer competitorPrice) {
		this.competitorPrice = competitorPrice;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Integer getDimensionScore() {
		return dimensionScore;
	}

	public void setDimensionScore(Integer dimensionScore) {
		this.dimensionScore = dimensionScore;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getDisplayScore() {
		return displayScore;
	}

	public void setDisplayScore(Integer displayScore) {
		this.displayScore = displayScore;
	}

	public List<PostUserCheckoutFarmer> getFarmers() {
		return farmers;
	}

	public void setFarmers(List<PostUserCheckoutFarmer> farmers) {
		this.farmers = farmers;
	}

	public PostUserCheckoutImage getImage() {
		return image;
	}

	public void setImage(PostUserCheckoutImage image) {
		this.image = image;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsBeverage() {
		return isBeverage;
	}

	public void setIsBeverage(Boolean isBeverage) {
		this.isBeverage = isBeverage;
	}

	public Boolean getIsDiscount() {
		return isDiscount;
	}

	public void setIsDiscount(Boolean isDiscount) {
		this.isDiscount = isDiscount;
	}

	public Boolean getIsProduct() {
		return isProduct;
	}

	public void setIsProduct(Boolean isProduct) {
		this.isProduct = isProduct;
	}

	public Boolean getIsSuperSensitive() {
		return isSuperSensitive;
	}

	public void setIsSuperSensitive(Boolean isSuperSensitive) {
		this.isSuperSensitive = isSuperSensitive;
	}

	public Integer getLabels() {
		return labels;
	}

	public void setLabels(Integer labels) {
		this.labels = labels;
	}

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getLabelDescription() {
		return labelDescription;
	}

	public void setLabelDescription(String labelDescription) {
		this.labelDescription = labelDescription;
	}

	public String getLabelDesc() {
		return labelDesc;
	}

	public void setLabelDesc(String labelDesc) {
		this.labelDesc = labelDesc;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Object getLastUpdateAt() {
		return lastUpdateAt;
	}

	public void setLastUpdateAt(Object lastUpdateAt) {
		this.lastUpdateAt = lastUpdateAt;
	}

	public Integer getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public List<String> getNextAvailableDates() {
		return nextAvailableDates;
	}

	public void setNextAvailableDates(List<String> nextAvailableDates) {
		this.nextAvailableDates = nextAvailableDates;
	}

	public String getPackDesc() {
		return packDesc;
	}

	public void setPackDesc(String packDesc) {
		this.packDesc = packDesc;
	}

	public String getPackNote() {
		return packNote;
	}

	public void setPackNote(String packNote) {
		this.packNote = packNote;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getSearchWords() {
		return searchWords;
	}

	public void setSearchWords(String searchWords) {
		this.searchWords = searchWords;
	}

	public Integer getStackPosition() {
		return stackPosition;
	}

	public void setStackPosition(Integer stackPosition) {
		this.stackPosition = stackPosition;
	}

	public Integer getStockAvailable() {
		return stockAvailable;
	}

	public void setStockAvailable(Integer stockAvailable) {
		this.stockAvailable = stockAvailable;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Double getWeightScore() {
		return weightScore;
	}

	public void setWeightScore(Double weightScore) {
		this.weightScore = weightScore;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getHealthBenefit() {
		return healthBenefit;
	}

	public void setHealthBenefit(String healthBenefit) {
		this.healthBenefit = healthBenefit;
	}

	public String getStoragePrep() {
		return storagePrep;
	}

	public void setStoragePrep(String storagePrep) {
		this.storagePrep = storagePrep;
	}

	public Boolean getIsRemoved() {
		return isRemoved;
	}

	public void setIsRemoved(Boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Boolean getIsOutOfStock() {
		return isOutOfStock;
	}

	public void setIsOutOfStock(Boolean isOutOfStock) {
		this.isOutOfStock = isOutOfStock;
	}

	public Object getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(Object errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}

}
