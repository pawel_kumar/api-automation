package userapipages.postusercheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutResult {
	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private PostUserCheckoutData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public PostUserCheckoutData getData() {
		return data;
	}

	public void setData(PostUserCheckoutData data) {
		this.data = data;
	}

}
