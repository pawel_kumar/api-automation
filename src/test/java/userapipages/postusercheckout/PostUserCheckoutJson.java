package userapipages.postusercheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutJson {
	@SerializedName("result")
	@Expose
	private PostUserCheckoutResult result;

	public PostUserCheckoutResult getResult() {
		return result;
	}

	public void setResult(PostUserCheckoutResult result) {
		this.result = result;
	}

}
