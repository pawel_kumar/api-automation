package userapipages.postusercheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutTotal {
	@SerializedName("totalPrice")
	@Expose
	private Integer totalPrice;
	@SerializedName("deliveryFee")
	@Expose
	private Integer deliveryFee;
	@SerializedName("isFreeDelivery")
	@Expose
	private Boolean isFreeDelivery;
	@SerializedName("discountAmount")
	@Expose
	private Integer discountAmount;

	public Integer getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getDeliveryFee() {
		return deliveryFee;
	}

	public void setDeliveryFee(Integer deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public Boolean getIsFreeDelivery() {
		return isFreeDelivery;
	}

	public void setIsFreeDelivery(Boolean isFreeDelivery) {
		this.isFreeDelivery = isFreeDelivery;
	}

	public Integer getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Integer discountAmount) {
		this.discountAmount = discountAmount;
	}

}
