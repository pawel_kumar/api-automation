package userapipages.postusercheckout;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUserCheckoutData {
	@SerializedName("items")
	@Expose
	private List<PostUserCheckoutItem> items = null;
	@SerializedName("total")
	@Expose
	private PostUserCheckoutTotal total;
	@SerializedName("checkoutTimestamp")
	@Expose
	private Double checkoutTimestamp;
	@SerializedName("isValid")
	@Expose
	private Boolean isValid;
	@SerializedName("personalData")
	@Expose
	private Object personalData;
	@SerializedName("orderNo")
	@Expose
	private String orderNo;
	@SerializedName("deliveryCode")
	@Expose
	private String deliveryCode;
	@SerializedName("deliveryDate")
	@Expose
	private String deliveryDate;

	public List<PostUserCheckoutItem> getItems() {
		return items;
	}

	public void setItems(List<PostUserCheckoutItem> items) {
		this.items = items;
	}

	public PostUserCheckoutTotal getTotal() {
		return total;
	}

	public void setTotal(PostUserCheckoutTotal total) {
		this.total = total;
	}

	public Double getCheckoutTimestamp() {
		return checkoutTimestamp;
	}

	public void setCheckoutTimestamp(Double checkoutTimestamp) {
		this.checkoutTimestamp = checkoutTimestamp;
	}

	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public Object getPersonalData() {
		return personalData;
	}

	public void setPersonalData(Object personalData) {
		this.personalData = personalData;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

}
