package userapipages;

import org.json.simple.JSONObject;

public class DeliverySelectionPage {

	public static JSONObject deliverySelectionCreateJson(String deliveryArea, String deliveryDate,
			String deliveryCode) {

		JSONObject requestParams = new JSONObject();
		requestParams.put("deliveryArea", deliveryArea);
		requestParams.put("deliveryDate", deliveryDate);
		requestParams.put("deliveryCode", deliveryCode);
		return requestParams;
	}

}
