package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserGetParamsData {
	@SerializedName("list")
	@Expose
	private java.util.List<GetUserGetParamsList> list = null;

	public java.util.List<GetUserGetParamsList> getList() {
		return list;
	}

	public void setList(java.util.List<GetUserGetParamsList> list) {
		this.list = list;
	}

}
