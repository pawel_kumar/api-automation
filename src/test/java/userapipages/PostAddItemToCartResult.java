package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostAddItemToCartResult {
	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private PostAddItemToCartData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public PostAddItemToCartData getData() {
		return data;
	}

	public void setData(PostAddItemToCartData data) {
		this.data = data;
	}
}
