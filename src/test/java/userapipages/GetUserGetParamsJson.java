package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserGetParamsJson {
	@SerializedName("result")
	@Expose
	private GetUserGetParamsResult result;

	public GetUserGetParamsResult getResult() {
		return result;
	}

	public void setResult(GetUserGetParamsResult result) {
		this.result = result;
	}

}
