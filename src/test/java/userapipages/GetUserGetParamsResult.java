package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserGetParamsResult {
	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private GetUserGetParamsData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetUserGetParamsData getData() {
		return data;
	}

	public void setData(GetUserGetParamsData data) {
		this.data = data;
	}

}
