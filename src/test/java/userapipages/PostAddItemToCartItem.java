package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostAddItemToCartItem {
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("quantity")
	@Expose
	private Integer quantity;
	@SerializedName("labelName")
	@Expose
	private String labelName;
	@SerializedName("displayName")
	@Expose
	private String displayName;
	@SerializedName("packDesc")
	@Expose
	private String packDesc;
	@SerializedName("price")
	@Expose
	private Integer price;
	@SerializedName("shortDesc")
	@Expose
	private String shortDesc;
	@SerializedName("stockAvailable")
	@Expose
	private Integer stockAvailable;
	@SerializedName("isDiscount")
	@Expose
	private Boolean isDiscount;
	@SerializedName("actualPrice")
	@Expose
	private Integer actualPrice;
	@SerializedName("maxQty")
	@Expose
	private Integer maxQty;
	@SerializedName("image")
	@Expose
	private PostAddItemToCartImage image;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPackDesc() {
		return packDesc;
	}

	public void setPackDesc(String packDesc) {
		this.packDesc = packDesc;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Integer getStockAvailable() {
		return stockAvailable;
	}

	public void setStockAvailable(Integer stockAvailable) {
		this.stockAvailable = stockAvailable;
	}

	public Boolean getIsDiscount() {
		return isDiscount;
	}

	public void setIsDiscount(Boolean isDiscount) {
		this.isDiscount = isDiscount;
	}

	public Integer getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(Integer actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Integer getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}

	public PostAddItemToCartImage getImage() {
		return image;
	}

	public void setImage(PostAddItemToCartImage image) {
		this.image = image;
	}

}
