package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostAddItemToCartSelected {
	@SerializedName("deliveryCode")
	@Expose
	private String deliveryCode;
	@SerializedName("deliveryDate")
	@Expose
	private String deliveryDate;
	@SerializedName("deliveryArea")
	@Expose
	private String deliveryArea;

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

}
