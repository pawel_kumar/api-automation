package userapipages;

import org.json.simple.JSONObject;
import org.testng.Assert;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PostAddItemToCartCreateRequest {

	public static JSONObject createJSON(String name, String quantity) {

		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("quantity", quantity);

		return requestParams;
	}

}
