package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInboxCampaignJson {
	@SerializedName("result")
	@Expose
	private GetInboxCampaignResult result;

	public GetInboxCampaignResult getResult() {
		return result;
	}

	public void setResult(GetInboxCampaignResult result) {
		this.result = result;
	}
}
