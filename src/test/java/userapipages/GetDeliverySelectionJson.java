package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDeliverySelectionJson {

	@SerializedName("result")
	@Expose
	private GetDeliverySelectionResults result;

	public GetDeliverySelectionResults getResult() {
		return result;
	}

	public void setResult(GetDeliverySelectionResults result) {
		this.result = result;
	}

}
