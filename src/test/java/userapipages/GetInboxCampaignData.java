package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInboxCampaignData {
	@SerializedName("list")
	@Expose
	private java.util.List<GetInboxCampaignList> list = null;

	public java.util.List<GetInboxCampaignList> getList() {
		return list;
	}

	public void setList(java.util.List<GetInboxCampaignList> list) {
		this.list = list;
	}

}
