package userapipages;

import org.json.simple.JSONObject;

import utilbase.TestUtil;

public class PostDeliverySelectionJson {

	public static JSONObject NewPostDeliverySelectionJson(String deliveryArea, String deliveryDate,
			String deliveryCode) {

		JSONObject requestParams = new JSONObject();
		requestParams.put("deliveryArea", deliveryArea);
		requestParams.put("deliveryCode", deliveryCode);
		requestParams.put("deliveryDate", deliveryDate);

		return requestParams;
	}
}
