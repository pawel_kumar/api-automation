package userapipages;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostAddItemToCartData {
	@SerializedName("selected")
	@Expose
	private PostAddItemToCartSelected selected;
	@SerializedName("items")
	@Expose
	private List<PostAddItemToCartItem> items = null;
	@SerializedName("total")
	@Expose
	private Integer total;

	public PostAddItemToCartSelected getSelected() {
		return selected;
	}

	public void setSelected(PostAddItemToCartSelected selected) {
		this.selected = selected;
	}

	public List<PostAddItemToCartItem> getItems() {
		return items;
	}

	public void setItems(List<PostAddItemToCartItem> items) {
		this.items = items;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
