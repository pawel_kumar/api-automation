package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserLastAddressCustomerInfo {
	@SerializedName("displayName")
	@Expose
	private String displayName;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("phone")
	@Expose
	private String phone;
	@SerializedName("providerId")
	@Expose
	private String providerId;
	@SerializedName("providerUid")
	@Expose
	private String providerUid;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderUid() {
		return providerUid;
	}

	public void setProviderUid(String providerUid) {
		this.providerUid = providerUid;
	}

}
