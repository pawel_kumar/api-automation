package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDeliverySelectionResults {

	@SerializedName("isSuccess")
	@Expose
	private Boolean isSuccess;
	@SerializedName("data")
	@Expose
	private GetDeliverySelectionData data;

	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetDeliverySelectionData getData() {
		return data;
	}

	public void setData(GetDeliverySelectionData data) {
		this.data = data;
	}

}
