package userapipages;

import org.json.simple.JSONObject;

public class PostUserCheckoutRequestJson {

	public static JSONObject createJSON(String uid, String provider) {

		JSONObject requestParams = new JSONObject();
		requestParams.put("uid", uid);
		requestParams.put("provider", provider);

		return requestParams;
	}
}
