package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserLastAddressData {
	@SerializedName("customerInfo")
	@Expose
	private GetUserLastAddressCustomerInfo customerInfo;
	@SerializedName("customerAddress")
	@Expose
	private GetUserLastAddressCustomerAddress customerAddress;

	public GetUserLastAddressCustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(GetUserLastAddressCustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public GetUserLastAddressCustomerAddress getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(GetUserLastAddressCustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}

}
