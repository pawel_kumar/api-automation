package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostAddItemToCartJson {

	@SerializedName("result")
	@Expose
	private PostAddItemToCartResult result;

	public PostAddItemToCartResult getResult() {
		return result;
	}

	public void setResult(PostAddItemToCartResult result) {
		this.result = result;
	}
}
