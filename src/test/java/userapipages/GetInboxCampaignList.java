package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInboxCampaignList {

	@SerializedName("imageUrl")
	@Expose
	private String imageUrl;
	@SerializedName("campaignType")
	@Expose
	private String campaignType;
	@SerializedName("subtitle")
	@Expose
	private String subtitle;
	@SerializedName("campaignName")
	@Expose
	private String campaignName;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("campaignStart")
	@Expose
	private Integer campaignStart;
	@SerializedName("updatedBy")
	@Expose
	private String updatedBy;
	@SerializedName("actionUrl")
	@Expose
	private String actionUrl;
	@SerializedName("createdBy")
	@Expose
	private String createdBy;
	@SerializedName("isActive")
	@Expose
	private Boolean isActive;
	@SerializedName("activatedBy")
	@Expose
	private String activatedBy;
	@SerializedName("uuid")
	@Expose
	private String uuid;
	@SerializedName("createdAt")
	@Expose
	private Integer createdAt;
	@SerializedName("campaignEnd")
	@Expose
	private Integer campaignEnd;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("updatedAt")
	@Expose
	private Integer updatedAt;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCampaignType() {
		return campaignType;
	}

	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCampaignStart() {
		return campaignStart;
	}

	public void setCampaignStart(Integer campaignStart) {
		this.campaignStart = campaignStart;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getActionUrl() {
		return actionUrl;
	}

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getActivatedBy() {
		return activatedBy;
	}

	public void setActivatedBy(String activatedBy) {
		this.activatedBy = activatedBy;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Integer createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getCampaignEnd() {
		return campaignEnd;
	}

	public void setCampaignEnd(Integer campaignEnd) {
		this.campaignEnd = campaignEnd;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Integer updatedAt) {
		this.updatedAt = updatedAt;
	}

}
