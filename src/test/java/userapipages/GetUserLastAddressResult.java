package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserLastAddressResult {
	@SerializedName("isSuccess")
	@Expose
	private String isSuccess;
	@SerializedName("data")
	@Expose
	private GetUserLastAddressData data;

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public GetUserLastAddressData getData() {
		return data;
	}

	public void setData(GetUserLastAddressData data) {
		this.data = data;
	}

}
