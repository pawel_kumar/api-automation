package userapipages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserLastAddressJson {
	@SerializedName("result")
	@Expose
	private GetUserLastAddressResult result;

	public GetUserLastAddressResult getResult() {
		return result;
	}

	public void setResult(GetUserLastAddressResult result) {
		this.result = result;
	}

}
