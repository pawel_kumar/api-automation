package apiinfo;

public class UserApiInfo {

	public final static String add_user_URL = "/v1/user/cart";
	public final static String get_delivery_selection_URL = "/v1/user/delivery-selection";
	public final static String delivery_selection_URL = "/v1/user/delivery-selection";
	public final static String UID = "PWAdsG9U5YXRxQgb3SUw1SY457g1";
	public final static String provider = "Firebase";
	public final static String PostUserCheckout = "/v1/user/checkout";
	public final static String GetUserGetParams = "/v1/user/params/rating/messages";
	public final static String GetUserLastAddress = "/v1/user/last-address";
	public final static String GetInboxCampaign = "/v1/user/inbox/campaigns";

}
