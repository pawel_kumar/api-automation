package apiinfo;

public class CatalogApiInfo {

	public final static String CatalogSectionsListURL = "/v1/catalog/sections";
	public final static String CatalogSectionsNameURL = "/v1/catalog/sections/default";

}
