package base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import apiinfo.UserApiInfo;
import utilbase.ErrorUtil;
import utilbase.TestUtil;

public class TestBase {
	public static Logger APP_LOGS = null;
	public static Properties CONFIG = null;
	public static Properties OR = null;
	public static utilbase.Xls_Reader suiteXls = null;
	public static utilbase.Xls_Reader UserSuite_xls = null;
	public static utilbase.Xls_Reader CatalogSuite_xls = null;
	public static utilbase.Xls_Reader suite_Home_xls = null;
	public static boolean isInitalized = false;
	public static boolean isBrowserOpened = false;
	public static Hashtable<String, String> sessionData = new Hashtable<String, String>();
	public UserApiInfo user_api_URL;
	public static WebDriver driver = null;
	// Extent Reports-
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	// helps to generate the logs in test report.
	public static ExtentTest test;
	public static String OS = "XYZ";
	public static String browser = "CHROME";

	// initializing the Tests
	public void initialize() throws Exception {
		// logs
		if (!isInitalized) {
			APP_LOGS = Logger.getLogger("devpinoyLogger");
			// config
			APP_LOGS.debug("Loading Property files");
			CONFIG = new Properties();
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//test//resources//config//config.properties");
			CONFIG.load(ip);
			OR = new Properties();
			ip = new FileInputStream(System.getProperty("user.dir") + "//src//test//resources//config//OR.properties");
			OR.load(ip);
			APP_LOGS.debug("Loaded Property files successfully");
			APP_LOGS.debug("Loading XLS Files");
			// xls file
			UserSuite_xls = new utilbase.Xls_Reader(
					System.getProperty("user.dir") + "//src//test//resources//xls//UserSuite.xlsx");
			CatalogSuite_xls = new utilbase.Xls_Reader(
					System.getProperty("user.dir") + "//src//test//resources//xls//CatalogSuite.xlsx");
			suite_Home_xls = new utilbase.Xls_Reader(
					System.getProperty("user.dir") + "//src//test//resources//xls//Home.xlsx");
			suiteXls = new utilbase.Xls_Reader(
					System.getProperty("user.dir") + "//src//test//resources//xls//Suite.xlsx");
			APP_LOGS.debug("Loaded XLS Files successfully");
			isInitalized = true;
		}
	}

	// selenium RC/ Webdriver
	// open a browser if its not opened
	public void openBrowser() {
		if (!isBrowserOpened) {
			if (CONFIG.getProperty("browserType").equals("MOZILLA")) {
				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "//src//com//selenium//driver//geckodriver.exe");
				driver = new FirefoxDriver();
			} else if (CONFIG.getProperty("browserType").equals("IE")) {
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "//src//com//selenium//driver//IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			} else if (CONFIG.getProperty("browserType").equals("CHROME")) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "//src//com//selenium//driver//chromedriver");
				driver = new ChromeDriver();
			}
			isBrowserOpened = true;
			String waitTime = CONFIG.getProperty("default_implicitWait");
			driver.manage().timeouts().implicitlyWait(Long.parseLong(waitTime), TimeUnit.SECONDS);
		}
	}

	// close browser
	public void closeBrowser() {
		driver.quit();
		isBrowserOpened = false;
	}

	// compare titles
	public boolean compareTitle(String expectedVal) {
		try {
			Assert.assertEquals(driver.getTitle(), expectedVal);
		} catch (Throwable t) {
			ErrorUtil.addVerificationFailure(t);
			APP_LOGS.debug("Titles do not match");
			return false;
		}
		return true;
	}

	// compaerStrings
	// compare titles
	public boolean compareNumbers(int expectedVal, int actualValue) {
		try {
			Assert.assertEquals(actualValue, expectedVal);
		} catch (Throwable t) {
			ErrorUtil.addVerificationFailure(t);
			APP_LOGS.debug("Values do not match");
			return false;
		}
		return true;
	}

	public boolean checkElementPresence(String xpathKey) {
		int count = driver.findElements(By.xpath(OR.getProperty(xpathKey))).size();

		try {
			Assert.assertTrue(count > 0, "No element present");
		} catch (Throwable t) {
			ErrorUtil.addVerificationFailure(t);
			APP_LOGS.debug("No element present");
			return false;
		}
		return true;
	}
	// your own functions
	// getObjectByID(String id)

	public WebElement getObject(String xpathKey) {

		try {
			WebElement x = driver.findElement(By.xpath(OR.getProperty(xpathKey)));
			return x;
		} catch (Throwable t) {
			// report error
			ErrorUtil.addVerificationFailure(t);
			APP_LOGS.debug("Cannot find object with key -- " + xpathKey);
			return null;
		}

	}

	// many companies
	public boolean login(String username, String password) {
		return true;
	}

	public void logout() {
	}

	public void capturescreenshot(String filename) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileHandler.copy(scrFile, new File(System.getProperty("user.dir") + "\\screenshots\\" + filename + ".jpg"));

	}

	public String getBaseURL(String type) {
		if (type.equalsIgnoreCase("user"))
			return CONFIG.getProperty("user_baseURL");
		else
			return CONFIG.getProperty("catalog_baseURL");
	}

	@BeforeSuite
	public void checkSuiteSkip() throws Exception {
		// Check Suite is runnable or not.
		initialize();
		APP_LOGS.debug("Checking Runmode of Shop Suite");
		if (!TestUtil.isSuiteRunnable(suiteXls, "UserSuite")) {
			APP_LOGS.debug("Skipped Shop Suite as the runmode was set to NO");
			throw new SkipException("RUnmode of Shop Suite set to no. So Skipping all tests in Suite A");
		}

		// extent Report Configuration.
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/testReport.html");
		// initialize ExtentReports and attach the HtmlReporter
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// To add system or environment info by using the setSystemInfo method.
		extent.setSystemInfo("OS", OS);
		extent.setSystemInfo("Browser", browser);
		extent.setSystemInfo("Tester", "Harish");
		extent.setSystemInfo("Server Info", "Dev Server");

		// configuration items to change the look and feel
		// add content, manage tests etc

		htmlReporter.config().setDocumentTitle("Extent Report Demo");
		htmlReporter.config().setReportName("Test Report");

		htmlReporter.config().setTheme(Theme.STANDARD);
		htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
	}

	@AfterSuite
	public void suiteTearDown() {
		extent.flush();
	}

}
